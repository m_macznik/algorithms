/*
 * armstrong_nums.c
 *
 *  Created on: 7 pa� 2019
 *      Author: Marek
 */

#include "armstrong_nums.h"

static unsigned int num_len_get(int num)
{
	int len = 0;
	while(num >= 1)
	{
		len++, num /= 10;
	}
	return len;
}

static unsigned int my_pow(int num, int power)
{
	unsigned int p = 1;
	for(unsigned int i = 0; i < power; i++)
	{
		p *= num;
	}
	return p;
}

static unsigned int num_digits_sum(int num)
{
	unsigned int num_len = num_len_get(num);
	char *txt = (char*)malloc(num_len * sizeof(char));
	sprintf(txt, "%d", num);
	unsigned int sum = 0;
	for(unsigned int i = 0; i < num_len; i++)
	{
		sum += my_pow( (*txt - 0x30), num_len );
		txt++;
	}
	free(txt);
	return sum;
}

unsigned char is_armstrong_num(int num)
{
	unsigned int digits_sum = num_digits_sum(num);
	return (num == digits_sum) ? 1 : 0;
}

unsigned char is_armstrong_test_cases_check(void)
{
	const unsigned int TEST_CASES_NUM = 4;
	unsigned int nums[] = {9, 10, 153, 154};
	unsigned int stats[] = {1, 0, 1, 0};
	for(unsigned int i = 0; i < TEST_CASES_NUM; i++)
	{
		if(stats[i] != is_armstrong_num( nums[i] ))
			return 0;
	}
	return 1;
}
