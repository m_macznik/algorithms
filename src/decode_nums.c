/*
 * decode_nums.c
 *
 *  Created on: 16 pa� 2019
 *      Author: Marek
 */

#include "decode_nums.h"
/*
 * Given the mapping a = 1, b = 2, ... z = 26, and an encoded message,
 * count the number of ways it can be decoded. For example, the message
 * '111' would give 3, since it could be decoded as 'aaa', 'ka', and 'ak'.
 */

char* test_cases_msg[] = { "111", "1324", "2613", "5183", "2123126" };
unsigned char test_cases_results[] = { 3, 4, 4, 2, 15 };

unsigned char ischar(unsigned short *num)
{
	return (*num > 26 || * num < 1) ? 0 : 1;
}


unsigned short decode_get_nb_combinations(char *msg)
{
	unsigned short nb = 1; // always one combination -> direct interpretation of digits
	unsigned int num = atoi(msg);
	unsigned int len = strlen(msg);
	if(len == 1)
		nb = 1;
	else if (len == 2)
		nb += ischar((unsigned short*)&num);
	else
	{
		nb = decode_get_nb_combinations(&msg[1]);
		num = (msg[0] - 0x30) * 10 + (msg[1] - 0x30);
		if(ischar((unsigned short*)&num))
		{
			num = atoi(&msg[2]);
			nb += decode_get_nb_combinations(&msg[2]);
		}
	}
	return nb;
}

unsigned char test_decode_msg_combinations_nb(void)
{
	unsigned char result = 0, i, comb_nb;
	unsigned short test_cases = sizeof(test_cases_results)/sizeof(test_cases_results[0]);
	for(i = 0; i < test_cases; i++)
	{
		comb_nb = decode_get_nb_combinations(test_cases_msg[i]);
		result += comb_nb == test_cases_results[i];
	}
	return result == test_cases;
}

