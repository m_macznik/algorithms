/*
 * quick_sort.c
 *
 *  Created on: 12 lis 2019
 *      Author: marek
 */


#include "quick_sort.h"

typedef struct sort_test_data
{
	uint32_t size;
	item_type_t *srcbuff;
	item_type_t *goalbuff;
}sort_test_data_t;

item_type_t d1[] = {4, 6, 1, 9, 3}, d2[] = {3, 2, 1},
		d3[] = {2011, 9160, 7629, 252, 1730, 9490, 94, 6291, 8641, 2034, 18, 791, 1372, 2595, 5014, 7096, 9866, 3910, 5059, 9885 },
		d4[] = {19, 52, 1, 920, 18, 0, 100}, d5[] = {19, 52, 1, 920, 18, 0, 100, 316, 792};

item_type_t g1[] = {1, 3, 4, 6, 9}, g2[] = {1, 2, 3},
		g3[] = { 18, 94, 252, 791, 1372, 1730,2011, 2034, 2595, 3910, 5014, 5059, 6291, 7096, 7629, 8641, 9160, 9490, 9866, 9885 },
		g4[] = {0, 1, 18, 19, 52, 100, 920}, g5[] = {0, 1, 18, 19, 52, 100, 316, 792, 920};


static inline void swap(item_type_t *i1, item_type_t *i2)
{
	item_type_t tmp = *i2;
	*i2 = *i1;
	*i1 = tmp;
}

static inline item_type_t* divide_dataset(item_type_t *left, item_type_t *right, item_type_t *pivot)
{
	item_type_t *l = left;
	item_type_t *r = right;

	while(1)
	{
		while( *l < *pivot) l++;
		while( r > 0 && *r > *pivot) r--;
		if( l >= r ) break;
		else swap(l,r);
	}
	return l;
}


static inline int8_t quick_sort_inner(item_type_t *left, item_type_t *right)
{
	item_type_t *l = left, *r = right, *pivot = r;
	item_type_t *div;
	if(r - l <= 0) return -1;
	div = divide_dataset(left, right, pivot);
	quick_sort_inner(left, div-1);
	quick_sort_inner(div+1, right);
	return 0;
}

int8_t quick_sort(item_type_t *dataset, uint32_t size)
{
	item_type_t *left = &dataset[0], *right = &dataset[size-1];
	return quick_sort_inner(left, right);
}


#define QS_TEST_CASES		5
uint8_t quick_sort_test(void)
{
	uint8_t result = 0, i, j;

	sort_test_data_t test_cases[QS_TEST_CASES] =
	{
			{ 5, d1, g1 }, { 3, d2, g2 }, { 15, d3, g3 },
			{ 7, d4,  g4 }, { 9, d5, g5 }
	};

	for(i = 0; i < QS_TEST_CASES; i++)
	{
		result = quick_sort(test_cases[i].srcbuff, test_cases[i].size);
		if(result != 0) return 0;
		for(j = 0; j < test_cases[i].size; j++)
		{
			result += test_cases[i].srcbuff[i] == test_cases[i].goalbuff[i];
		}
		if(result != test_cases[i].size) return 0;
	}

	return 1;
}
