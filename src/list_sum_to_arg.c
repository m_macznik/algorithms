/*
 * list_sum_to_arg.c
 *
 *  Created on: 9 pa� 2019
 *      Author: Marek
 */

#include "list_sum_to_arg.h"
/**
 * Given a list of numbers and a number k. Return
 * whether any two numbers from the list sum up to k.
 *
 * [10, 15, 3, 7], 17. return true since 10 + 7 = 17
 */

void list_remove_duplicate(list_t *src, list_t *dst)
{
	unsigned int new_size = 0, j;
	memset(dst->items, 0, sizeof(list_t));
	for(unsigned int i = 0; i < src->size; i++)
	{
		for(j = 0; j < new_size; j++)
		{
			if(src->items[i] == dst->items[j])
			{
				break;
			}
		}
		if(j == new_size)
		{
			dst->items[new_size++] = src->items[i];
		}
	}
	dst->size = new_size;
	realloc(dst->items, new_size * sizeof(unsigned int));
}

void list_remove_element_gt(list_t *list, long long *k)
{
	unsigned int position = 0;
	for(unsigned int i = 0; i < list->size; i++)
	{
		if(list->items[i] >= *k)
		{
			break;
		}
		position++;
	}
	list->size = position;
	realloc(list->items, list->size * sizeof(unsigned int));
}

int comparsion_func(const void * a, const void * b)
{
    int _a = *(int*)a;
    int _b = *(int*)b;
    if(_a < _b) return -1;
    else if(_a == _b) return 0;
    else return 1;
}

void list_sort(list_t *list, sort_type_t SORT_TYPE)
{
	switch(SORT_TYPE)
	{
		case C_STANDARD_QSORT:
			qsort(list->items, list->size, sizeof(list->items[0]), comparsion_func);
			break;
		default:
			break;
	}
}

unsigned char begin_end_sum_cmp(list_t *list, long long k)
{
	unsigned int *begin = (unsigned int*)list->items;
	unsigned int *end = (unsigned int*)&list->items[list->size-1];
	unsigned int *mid_point = (unsigned int*)&list->items[list->size / 2];
	long long sum;
	while(begin <= mid_point || end >= mid_point)
	{
		sum = *begin + *end;
		if( sum == k )
			return 1;
		else if(sum > k)
			end--;
		else if(sum < k)
			begin++;
	}
	return 0;
}

unsigned char sum_check(list_t *list, long long k)
{
	list_t tmp_list;
	unsigned char result = 0;
	tmp_list.items = (unsigned int*)malloc( list->size * sizeof(unsigned int) );
	tmp_list.size = list->size;
	list_remove_duplicate(list, &tmp_list);
	list_remove_element_gt(&tmp_list, &k);
	list_sort(&tmp_list, C_STANDARD_QSORT);
	result = begin_end_sum_cmp(&tmp_list, k);
	free(tmp_list.items);
	return result;
}
