/*
 * xor_linked_list.c
 *
 *  Created on: 14 pa� 2019
 *      Author: Marek
 */

/*
 * Implement a XOR linked list:
 * - add_item(element)
 * - get_item(index)
 * - traverse -> prints to standard output all items in list
 * OPTIONAL: - count variable which stores amount of items in list
 */
#include "xor_linked_list.h"

void add_item(list_item_t item);
list_node_t get_item(unsigned int index);
void print_list(list_node_t *head);
void remove_item(unsigned char from_end);

static inline list_node_t* node_xor(list_node_t *a, list_node_t *b)
{
	return (list_node_t*)((uintptr_t)a ^ (uintptr_t)b);
}

list_handler_t list_handler = {
		0,
		(void*)0,
		(void*)0,
		add_item,
		get_item,
		print_list,
		remove_item
};

void add_item(list_item_t item)
{
	list_node_t *node = (list_node_t*)malloc(sizeof(list_node_t));
	node->item = item;
	node->npx = node_xor(list_handler.curr_pos, (void*)0);
	if( list_handler.curr_pos != (void*)0 )
	{
		list_node_t *next = node_xor(list_handler.curr_pos->npx, (void*)0);
		list_handler.curr_pos->npx = node_xor(node, next);
	}
	if(list_handler.head == (void*)0)
		list_handler.head = node;
	list_handler.curr_pos = node;
	list_handler.count++;
}

list_node_t get_item(unsigned int index)
{
	list_node_t node = {0, (void*)0};
	list_node_t *curr = list_handler.head;
	list_node_t *next = (void*)0, *prev = (void*)0;
	unsigned int i;
	if(index > list_handler.count)
		return node; // empty
	for(i = 0; i < index; i++)
	{
		next = node_xor(prev, curr->npx);
		prev = curr;
		curr = next;
	}
	node = *curr;
	return node;
}

void remove_item(unsigned char from_end)
{
	list_node_t *ptr = (void*)0, *prev = (void*)0,
			*next = (void*)0;
	if(list_handler.count == 0)
		return;
	if(from_end)
	{
		ptr = list_handler.curr_pos;
		prev = node_xor(ptr->npx, (void*)0);
		if(prev == (void*)0)
		{
			list_handler.head = (void*)0;
		}
		else
		{
			prev->npx = node_xor(ptr, node_xor(prev->npx, (void*)0));
		}
		list_handler.curr_pos = prev;
	}
	else
	{
		ptr = list_handler.head;
		next = node_xor(ptr->npx, (void*)0);
		if(next == (void*)0)
		{
			list_handler.curr_pos = (void*)0;
		}
		else
		{
			next->npx = node_xor(ptr, node_xor( (void*)0, next->npx) );
		}
		list_handler.head = next;
	}
	free(ptr);
	ptr = (void*)0;
	list_handler.count--;
}

void print_list(list_node_t *head)
{
	list_node_t *curr = list_handler.head,
			*prev = (void*)0, *next = (void*)0;
	while(curr != (void*)0)
	{
		printf("%d\n", curr->item);
		next = node_xor(prev, curr->npx);
		prev = curr;
		curr = next;
	}
}

unsigned char list_check(void)
{
	list_node_t node;
	unsigned int repeats;
	unsigned int cnt = 0;
	unsigned int vals[] = {10, 20, 30, 40};
	for(unsigned int i = 0; i <4; i++)
	{
		list_handler.add_item(vals[i]);
	}
	cnt += list_handler.count == sizeof(vals)/sizeof(vals[0]);
	for(unsigned int i = 0; i < list_handler.count; i++)
	{
		node = list_handler.get_item_at_index(i);
		cnt += node.item == vals[i];
	}
	repeats = list_handler.count;
	for(unsigned int i = 0; i < repeats; i++)
	{
		list_handler.remove_item(1);
	}
	cnt += list_handler.count == 0;
	return cnt == 6;
}
