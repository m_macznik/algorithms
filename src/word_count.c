/*
 * word_count.c
 *
 *  Created on: 22 pa� 2019
 *      Author: Marek
 */

#include "word_count.h"


#define WORD_LEN 6 // longer strings are hard to manage in memory
int word_counts[] = {2, 1, 1, 1};

void normalize_text(char *msg)
{
	int len = strlen(msg);
	while(*msg && len--)
	{
		*msg = toupper(*msg);
		msg++;
	}
}

void display_text(char *text)
{
	printf("%s\n", text);
}

unsigned int word_list_create(char *input, char *output)
{
	char word[WORD_LEN] = {0};
	char *c = input;
	char *w = word;
	unsigned int words = 0;
	unsigned int word_len = 0;
	while(*c)
	{
		if( isalpha(*c) )
		{
			*w++ = *c;
			word_len++;
		}
		else
		{
			memset(output + words * WORD_LEN, 0, WORD_LEN);
			for(int i = 0; i < word_len; i++)
				*(output + words * WORD_LEN + i) = word[i];
			word_len = 0;
			words++;
			w = word;
			realloc(output, (words + 1) * WORD_LEN * sizeof(char));
		}
		c++;
	}
	return words;
}

int* count_occurences(char *word_list, unsigned int *words)
{
	int i, j;
	int *occur = (int*)calloc(*words, sizeof(int));
	int count;
	char *s1, *s2;
	for(i = 0; i < *words; i++) occur[i] = 0;

	for(i = 0; i < *words; i++)
	{
		count = 1;
		s1 = word_list + i * WORD_LEN;
		for(j = i + 1; j < *words; j++)
		{
			s2 = word_list + j * WORD_LEN;
			if(strcmp(s1, s2) == 0)
				count++;
		}
		if(i < *words - 1) occur[i] = count;
	}

	return occur;
}

int* get_words_nb(char *text)
{
	unsigned int len = strlen(text);
	if(len == 0) return 0;
	unsigned int words = 0;
	int *occur;
	char *word_list = (char *)calloc(WORD_LEN, sizeof(char));
	memset(word_list, 0, WORD_LEN * sizeof(char));

	words = word_list_create(text, word_list);
	occur = count_occurences(word_list, &words);

	free(word_list);
	return occur;
}

unsigned char word_count_test(void)
{
	char text[] = "olly olly in come free ";
	int *occur = (void*)0;
	unsigned char result = 0;
	int i, c;
	int size = sizeof(word_counts) / sizeof(word_counts[0]);
	normalize_text(text);
	occur = get_words_nb(text);

	for(i = 0; i < size; i++)
	{
		c = occur[i];
		result += c == word_counts[i];
	}

	free(occur);
	if (result == size)
		return 1;
	else
		return 0;
}
