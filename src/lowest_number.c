/*
 * lowest_number.c
 *
 *  Created on: 12 pa� 2019
 *      Author: Marek
 */

#include "lowest_number.h"

/**
 * Given an array of integers, find the first missing positive
 * integer in linear time and constant space. In other words,
 * find the lowest positive integer that does not exist in the
 * array. The array can contain duplicates and negative numbers as well.
 *
 * For exaple, input: 	[3, 4, -1, 1] should give 2 and
 * 			input:		[1, 2, 0] should give 3.
 *
 */

#define TEST_CASES		2

void print_array(int *array, int *size)
{
	for(unsigned int i = 0; i < *size; i++)
	{
		printf("%d\t", array[i]);
	}
	printf("\n");
}

static void remove_duplicates(int *in, int *in_size, int *out, int *out_size)
{
	int new_size = 0, j;
	for(unsigned int i = 0; i < *in_size; i++)
	{
		for(j = 0; j < new_size; j++)
		{
			if(in[i] == out[j])
				break;
		}
		if(j == new_size)
		{
			out[new_size++] = in[i];
		}
	}
	*out_size = new_size;
	realloc(out, new_size * sizeof(int));
}

static int cmp_func(const void * a, const void * b)
{
    int _a = *(int*)a;
    int _b = *(int*)b;
    if(_a < _b) return -1;
    else if(_a == _b) return 0;
    else return 1;
}

static void array_sort(int *array, int *size)
{
	qsort(array, *size, sizeof(array[0]), cmp_func);
}

static void delete_zero_negatives(int *array, int *size)
{
	int position = 0;
	for(unsigned int i = 0; i < *size; i++)
	{
		if(array[i] <= 0)
			position++;
		else
			break;
	}
	for(unsigned int i = position; i < *size; i++)
	{
		array[i - position] = array[i];
	}
	*size = position + 1;
	realloc(array, *size * sizeof(int));
}

static inline unsigned int find_lowest_missing_int(int *array, int *size)
{
	unsigned int lowest = 0;
	unsigned int i;
	for(i = 1; i < *size; i++)
	{
		if(array[i] - array[i-1] > 1)
		{
			lowest = array[i-1] + 1;
		}
	}
	if(lowest == 0)
		lowest = array[*size-1] + 1;
	return lowest;
}

unsigned int missing_positive_integer(int *in, int *in_size, int *out, int *out_size)
{
	remove_duplicates(in, in_size, out, out_size);
	array_sort(out, out_size);
	delete_zero_negatives(out, out_size);
	return find_lowest_missing_int(out, out_size);
}

unsigned char missing_positive_integer_test_cases(void)
{
	unsigned char result = 0;
	int ta1[] = {3, 4, -1, 0, 1, 1}, ta1_size = sizeof(ta1)/sizeof(ta1[0]);
	int ta2[] = {2, 1, 0, 2}, ta2_size = sizeof(ta2)/sizeof(ta2[0]);
	int *out = NULL, out_size = 0;
	int *test_arrays[] = {ta1, ta2};
	int test_sizes[] = {ta1_size, ta2_size};
	int test_lowest[] = {2, 3};
	int lowest;
	for(unsigned int i = 0; i < TEST_CASES; i++)
	{
		out_size = test_sizes[i];
		out = (int*)calloc(out_size, sizeof(int));
		lowest = missing_positive_integer(test_arrays[i], &test_sizes[i],
				out, &out_size);
		lowest == test_lowest[i] ? result++ : result;
		free(out);
	}
	return result == TEST_CASES;
}
