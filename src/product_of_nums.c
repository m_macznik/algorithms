/*
 * product_of_nums.c
 *
 *  Created on: 12 pa� 2019
 *      Author: Marek
 */

#include "product_of_nums.h"
/**
 * Given an array of intergers. Return a new array such that
 * each element at index i of the new array is the product
 * of all the numbers in the original array except the one at i.
 *
 * For example: [1, 2, 3, 4, 5] should give: [120, 60, 40, 30 ,24] and
 * 				[3, 2, 1] should give: [2, 3, 6].
 *
 * Follow up: Not using division
 */

#define TEST_CASES 2
unsigned char test_products(void)
{
	unsigned int a1[] = {1, 2, 3, 4, 5}, r1[] = {120, 60, 40, 30, 24};
	unsigned int a2[] = {3, 2, 1}, r2[] = {2, 3, 6};
	array_t in[TEST_CASES] = {
			{a1, sizeof(a1)/sizeof(a1[0])},
			{a2, sizeof(a2)/sizeof(a2[0])}
	};
	array_t out[TEST_CASES] = {
			{r1, sizeof(r1)/sizeof(r1[0])},
			{r2, sizeof(r2)/sizeof(r2[0])}
	};
	array_t prod = {(void*)0, 0};
	unsigned char result = 0;
	for(unsigned char i = 0; i < TEST_CASES; i++)
	{
		prod.size = in[i].size;
		prod.items = (unsigned int*)malloc(prod.size * sizeof(unsigned int));
		calculate_items_product(&in[i], &prod);
		compare_arrays(&prod, &out[i]) == 1 ? result++ : result;
		free(prod.items);
	}
	return result == TEST_CASES;
}

unsigned char compare_arrays(array_t *i1, array_t *i2)
{
	unsigned int i, size = i1->size;
	for(i = 0; i < size; i++)
	{
		if(i1->items[i] != i2->items[i]) return 0;
	}
	return 1;
}

void calculate_items_product(array_t *input, array_t *output)
{
	unsigned int prod;
	for(unsigned int i = 0; i < input->size; i++)
	{
		prod = 1;
		for(unsigned int j = 0; j < input->size; j++)
		{
			prod *= &input->items[i] == &input->items[j] ? 1 : input->items[j];
		}
		output->items[i] = prod;
	}
}
