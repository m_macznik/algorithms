/*
 * list_sum_to_arg.h
 *
 *  Created on: 9 pa� 2019
 *      Author: Marek
 */

#ifndef INC_LIST_SUM_TO_ARG_H_
#define INC_LIST_SUM_TO_ARG_H_

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

typedef enum sort_type
{
	C_STANDARD_QSORT
}sort_type_t;

typedef struct list
{
	unsigned int *items;
	unsigned int size;
}list_t;

unsigned char sum_check(list_t *list, long long k);
void list_remove_duplicate(list_t *src, list_t *dst);
void list_remove_element_gt(list_t *list, long long *k);
int comparsion_func(const void * a, const void * b);
void list_sort(list_t *list, sort_type_t SORT_TYPE);

#endif /* INC_LIST_SUM_TO_ARG_H_ */
