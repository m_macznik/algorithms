/*
 * product_of_nums.h
 *
 *  Created on: 12 pa� 2019
 *      Author: Marek
 */

#ifndef INC_PRODUCT_OF_NUMS_H_
#define INC_PRODUCT_OF_NUMS_H_

#include "list_sum_to_arg.h"
typedef list_t array_t;

unsigned char compare_arrays(array_t *i1, array_t *i2);
unsigned char test_products(void);
void calculate_items_product(array_t *input, array_t *output);

#endif /* INC_PRODUCT_OF_NUMS_H_ */
