/*
 * armstrong_nums.h
 *
 *  Created on: 7 pa� 2019
 *      Author: Marek
 */

#ifndef INC_ARMSTRONG_NUMS_H_
#define INC_ARMSTRONG_NUMS_H_
#include <stdlib.h>
#include <stdio.h>

unsigned char is_armstrong_num(int num);
unsigned char is_armstrong_test_cases_check(void);

#endif /* INC_ARMSTRONG_NUMS_H_ */
