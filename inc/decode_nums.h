/*
 * decode_nums.h
 *
 *  Created on: 16 pa� 2019
 *      Author: Marek
 */

#ifndef INC_DECODE_NUMS_H_
#define INC_DECODE_NUMS_H_
#include <string.h>
#include <stdlib.h>

unsigned char test_decode_msg_combinations_nb(void);

#endif /* INC_DECODE_NUMS_H_ */
