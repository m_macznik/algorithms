/*
 * word_count.h
 *
 *  Created on: 22 pa� 2019
 *      Author: Marek
 */

#ifndef INC_WORD_COUNT_H_
#define INC_WORD_COUNT_H_

#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

unsigned char word_count_test(void);

#endif /* INC_WORD_COUNT_H_ */
