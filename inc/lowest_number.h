/*
 * lowest_number.h
 *
 *  Created on: 12 pa� 2019
 *      Author: Marek
 */

#ifndef INC_LOWEST_NUMBER_H_
#define INC_LOWEST_NUMBER_H_
#include <stdlib.h>
#include <stdio.h>

unsigned int missing_positive_integer(int *in, int *in_size, int *out, int *out_size);
unsigned char missing_positive_integer_test_cases(void);

#endif /* INC_LOWEST_NUMBER_H_ */
