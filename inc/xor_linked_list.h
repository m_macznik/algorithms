/*
 * xor_linked_list.h
 *
 *  Created on: 14 pa� 2019
 *      Author: Marek
 */

#ifndef INC_XOR_LINKED_LIST_H_
#define INC_XOR_LINKED_LIST_H_

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

#define XOR_LIST_TYPE	int
typedef XOR_LIST_TYPE	list_item_t;

typedef struct list_node
{
	list_item_t item;
	struct list_node *npx;
}list_node_t;


typedef struct list_handler
{
	unsigned int count;
	list_node_t *curr_pos;
	list_node_t *head;
	void (*add_item)(list_item_t);
	list_node_t (*get_item_at_index)(unsigned int);
	void (*print_content)(list_node_t *);
	void (*remove_item)(unsigned char);
}list_handler_t;

extern list_handler_t list_handler;
unsigned char list_check(void);

#endif /* INC_XOR_LINKED_LIST_H_ */
