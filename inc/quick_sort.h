/*
 * quick_sort.h
 *
 *  Created on: 12 lis 2019
 *      Author: marek
 */

#ifndef INC_QUICK_SORT_H_
#define INC_QUICK_SORT_H_

#include <stdint.h>
#include <math.h>

typedef int32_t item_type_t;
int8_t quick_sort(item_type_t *dataset, uint32_t size);
uint8_t quick_sort_test(void);

#endif /* INC_QUICK_SORT_H_ */
