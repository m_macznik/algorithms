/*
 * tasks.c
 *
 *  Created on: 22 pa� 2019
 *      Author: Marek
 */


#include "tasks.h"

void tasks_check(void)
{
	unsigned int buff[] = {2, 1, 1, 1, 5, 7, 3, 10};
	list_t list = { buff, sizeof(buff)/sizeof(buff[0]) };

	unsigned char result = is_armstrong_test_cases_check();
	printf("Armstrong numbers: %s!\n", RESULT_CHECK(result));
	result = sum_check(&list, 10);
	printf("List sum check: %s!\n", RESULT_CHECK(result));
	result = test_products();
	printf("Items product: %s!\n", RESULT_CHECK(result));
	result = missing_positive_integer_test_cases();
	printf("Missing positive integer: %s!\n", RESULT_CHECK(result));
	result = list_check();
	printf("XOR list: %s!\n", RESULT_CHECK(result));
	result = test_decode_msg_combinations_nb();
	printf("Decode posibilities: %s!\n", RESULT_CHECK(result));
//	result = word_count_test();
//	printf("Word count: %s!\n", RESULT_CHECK(result));
	result = quick_sort_test();
	printf("Quick sort test: %s!\n", RESULT_CHECK(result));
}
