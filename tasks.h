/*
 * tasks.h
 *
 *  Created on: 22 pa� 2019
 *      Author: Marek
 */

#ifndef INC_TASKS_H_
#define INC_TASKS_H_

#include "armstrong_nums.h"
#include "list_sum_to_arg.h"
#include "product_of_nums.h"
#include "lowest_number.h"
#include "xor_linked_list.h"
#include "decode_nums.h"
#include "word_count.h"
#include "quick_sort.h"

#define		RESULT_CHECK(x)		(x == 1 ? "SUCCESS" : "FAILED")

void tasks_check(void);

#endif /* INC_TASKS_H_ */
